﻿using MVCajaxTest.ViewModels;
using System.Web.Mvc;

namespace MVCajaxTest.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var homeViewModel = new HomeViewModel();
            homeViewModel.GetCountries();

            return View(homeViewModel);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public string GetSecuredData(string userName, string password)
        {
            string securedInfo = "";
            if ((userName == "admin") && (password == "admin"))
                securedInfo = "Hello admin, your secured information is .......";
            else
                securedInfo = "Wrong username or password, please retry.";
            return securedInfo;
        }

        [HttpPost]
        public JsonResult GetCountrySubdivisions(string countryCode)
        {
            return Json(HomeViewModel.GetCountrySubdivisions(countryCode), JsonRequestBehavior.AllowGet);
        }

    }
}