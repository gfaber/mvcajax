﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace MVCajaxTest.ViewModels
{
    public class HomeViewModel
    {

        public string Country { get; set; }

        public string CountrySubdivision { get; set; }
        public List<Country> Countries { get; set; }
        public List<CountrySubdivision> CountrySubdivisions { get; set; }

        public string GetCountries()
        {
            string returnMessage = string.Empty;
            using (var context = new TestDataEntities())
            {
                Countries = context.Countries.OrderBy(x => x.CountryDescription).ToList();
            }
            return returnMessage;
        }

        public static List<CountrySubdivision> GetCountrySubdivisions(string countryCode)
        {
            var countrySubdivisions = new List<CountrySubdivision>();
            using (var context = new TestDataEntities())
            {
                context.Configuration.LazyLoadingEnabled = false;
                countrySubdivisions = context.CountrySubdivisions.Where(x => x.CountryCode == countryCode).OrderBy(x => x.SubdivisionDescription).ToList();
            }
            return countrySubdivisions;
        }
    }
}